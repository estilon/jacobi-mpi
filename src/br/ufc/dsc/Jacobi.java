package br.ufc.dsc;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.sun.javafx.scene.control.skin.DoubleFieldSkin;
import com.sun.javafx.scene.paint.GradientUtils;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class Jacobi {

	private float[][] matrixA;
	private float[] matrixB;
	private double[] matrixC;
	private Gson gson;

	private  int numeroDeProcessos;
	private int c = 0;
	
	public Jacobi(float[][] matrixA, float[] matrixB){
		this.matrixA = matrixA;
		this.matrixB = matrixB;
		this.matrixC = new double[matrixB.length];
		gson = new Gson();
	}
	
	public double[] raiz(){
		iniciarC();
		iterar();
		return matrixC.clone();
	}
	
	private void iniciarC(){
		for(int i = 0; i < matrixC.length; i++){
			matrixC[i] = 0;
		}
	}
	
	/*private void iterar(){
		
		float[] temp = new float[matrixC.length];
		int c = 0;
		while(c++ < 10000){
			for(int i = 0; i < matrixA.length; i++){
				float a = 0;
				for(int j = 0; j < matrixA.length; j++){
					a += i == j ? (matrixB[i])/matrixA[i][i] : (matrixA[i][j]/(-matrixA[i][i])) * matrixC[j];
				}
				temp[i] =  a;
			}

			for(int i = 0; i < matrixA.length; i++){

				matrixC[i] = temp[i];

			}

		}
		
		verificar();
	}*/

	private void iterar(){

		try {
			int receberamA = 0;
			numeroDeProcessos = MPI.COMM_WORLD.getSize() - 1;
			int resto = matrixA.length % numeroDeProcessos;
			int linhasPorProcesso = matrixA.length / numeroDeProcessos;
			while(receberamA < MPI.COMM_WORLD.getSize()-1) {

				Status status = MPI.COMM_WORLD.recv(new int[1], 1, MPI.INT, MPI.ANY_SOURCE, Constantes.REQUISITAR_MATRIZ_A);
				//System.out.println("Enviando matrix A");

				String json = gson.toJson(matrixA);
				MPI.COMM_WORLD.send(json.toCharArray(), json.toCharArray().length, MPI.CHAR, status.getSource(), Constantes.ENVIAR_MATRIZ_A);
				//System.out.println(status.getSource());

				status = MPI.COMM_WORLD.recv(new int[1], 1, MPI.INT, MPI.ANY_SOURCE, Constantes.REQUISITAR_MATRIZ_B);
				//System.out.println("Enviando matrix B");
				json = gson.toJson(matrixB);
				MPI.COMM_WORLD.send(json.toCharArray(), json.toCharArray().length, MPI.CHAR, status.getSource(), Constantes.ENVIAR_MATRIZ_B);

				//System.out.println(receberamA + " " + ((receberamA + 1)*linhasPorProcesso + resto - 1));

				status = MPI.COMM_WORLD.recv(new float[1], 1, MPI.FLOAT, MPI.ANY_SOURCE, Constantes.REQUISITAR_LINHA);
				if(receberamA < MPI.COMM_WORLD.getSize()-2){
					int[] linha = {receberamA*linhasPorProcesso , (receberamA + 1)*linhasPorProcesso - 1};
					MPI.COMM_WORLD.send(linha, 2, MPI.INT, status.getSource(), Constantes.ENVIAR_LINHA);
				}else{
					int[] linha = {receberamA*linhasPorProcesso, (receberamA + 1)*linhasPorProcesso + resto - 1};
					MPI.COMM_WORLD.send(linha, 2, MPI.INT, status.getSource(), Constantes.ENVIAR_LINHA);
				}

				receberamA++;
			}
		} catch (MPIException e) {
			e.printStackTrace();
		}

		while(c++ < Main.maxNumberOfInteractions){
			//System.out.println("Começando iteração " + c);

			int respostas = 0;

			try {
				for(int i = 0; i < MPI.COMM_WORLD.getSize() - 1; i++){
					//System.out.println("Aguardando que alguém requisite a matriz C");
					Status status = MPI.COMM_WORLD.recv(new float[1], 1, MPI.FLOAT, MPI.ANY_SOURCE, Constantes.REQUISITAR_MATRIZ_C);


					//System.out.println(dateFormat.format(new Date()) + " Enviando matrix C " + c + " para processo "+status.getSource());
					if(c < 10001) {
						MPI.COMM_WORLD.send(matrixC, matrixC.length, MPI.DOUBLE, status.getSource(), Constantes.ENVIAR_MATRIZ_C);
					}
					else {
						float[] temp = {0};
						MPI.COMM_WORLD.send(temp, 1, MPI.DOUBLE, status.getSource(), Constantes.ENVIAR_MATRIZ_C);
					}

                }
			} catch (MPIException e) {
				e.printStackTrace();
			}

			while(respostas < numeroDeProcessos && c < Main.maxNumberOfInteractions + 1) {
				try {


					double[] temp = new double[100000];

					Status status = MPI.COMM_WORLD.recv(temp, temp.length, MPI.DOUBLE, MPI.ANY_SOURCE, Constantes.RECEBER_RESPOSTA);

					//System.out.println("Na iteracao " + c + " Recebendo resposta de " + status.getSource() + " para linha " + resposta.getInicio());

					int inicio = (new Double(temp[0]).intValue());
					int fim = (new Double(temp[1]).intValue());
					for(int i = 2, j = inicio; j <= fim; i++, j++){
						matrixC[j] = temp[i];
					}
					respostas++;

				} catch (MPIException e) {
					e.printStackTrace();
				}
			}

			//System.out.println("Acabando iteração. Verificando respostas");
			if(verificar()){
				break;
			}

		}
		pararProcessos();

	}

	private void pararProcessos() {
		try {
			for(int i = 0; i < MPI.COMM_WORLD.getSize() - 1; i++){
                //System.out.println("Aguardando que alguém requisite a matriz C");
                Status status = MPI.COMM_WORLD.recv(new float[1], 1, MPI.FLOAT, MPI.ANY_SOURCE, Constantes.REQUISITAR_MATRIZ_C);


                //System.out.println(" Enviando matrix C " + c + " para processo "+status.getSource());
				float[] temp = {0};
				MPI.COMM_WORLD.send(temp, 1, MPI.DOUBLE, status.getSource(), Constantes.ENVIAR_MATRIZ_C);

            }
		} catch (MPIException e) {
			e.printStackTrace();
		}
	}


	public boolean verificarCongruencia(){
		boolean converge = true;
		for(int i = 0; i < matrixA.length; i++){
			int soma = 0;

			for(int j = 0; j < matrixA.length; j++){
				soma += i == j ? 0 : matrixA[i][j] ;
			}

			if(((0.1) * soma)/matrixA[i][i] >= 1){
				converge = false;
			}
		}

		if(converge) return true;

		for(int i = 0; i < matrixA.length; i++){
			int soma = 0;
			for(int j = 0; j < matrixA.length; j++){
				soma += i == j ? 0 : matrixA[j][i];
			}

			if(((0.1) * soma)/matrixA[i][i] >= 1){
				converge = false;
			}
		}
		return  converge;
	}
	
	private boolean verificar(){
		boolean retorno = true;
		for(int i = 0; i < matrixA.length; i++){
			double soma = 0;
			for(int j = 0; j < matrixA.length; j++){
				soma += matrixA[i][j] * matrixC[j];
			}
			//System.out.println("Soma "+soma + ", coisa lá: "+matrixB[i] + " diferenca = " + (matrixB[i] - soma)  );
			if(Math.abs(matrixB[i] - soma) < 0.1){
				//System.out.println("Corrert!");
				retorno = true;
			}else{
				//System.out.println("No correct");
				return false;
			}
		}
		return retorno;
	}

	public int iteracoes(){
		return --c;
	}

	public float diferenca(int i){
		float soma = 0.0f;
		for(int j = 0; j < matrixA.length; j++){
			soma += matrixA[i][j] * matrixC[j];
		}

		return matrixB[i] - soma;
	}

}
