package br.ufc.dsc;

/**
 * Created by estilon on 04/07/16.
 */
public class Mensagem {
    private int inicio;
    private int fim;
    private double[] resposta;

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    public double[] getResposta() {
        return resposta.clone();
    }

    public void setResposta(double[] resposta) {
        this.resposta = resposta.clone();
    }

    public int getFim() {
        return fim;
    }

    public void setFim(int fim) {
        this.fim = fim;
    }
}


