package br.ufc.dsc;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by estilon on 20/06/16.
 */
public class Solver{


    private Gson gson;
    private float[][] matrixA;
    private float[] matrixB;
    private double[] matrixC;
    private int inicio;
    private int fim;

    public Solver(){
        gson = new Gson();
        matrixC = new double[100000];
        try {
            char[] buffer = new char[10000000];
            MPI.COMM_WORLD.send(new int[1], 1, MPI.INT, Constantes.PROCESSO_MASTER, Constantes.REQUISITAR_MATRIZ_A);

            //System.out.println("Esperando resposta");
            Status status = MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.CHAR, Constantes.PROCESSO_MASTER, Constantes.ENVIAR_MATRIZ_A);
            JsonReader reader = new JsonReader(new StringReader(new String(buffer)));
            reader.setLenient(true);
            matrixA = gson.fromJson(reader,float[][].class);

            //System.out.println("Esperando matrix B");
            MPI.COMM_WORLD.send(new float[1], 1, MPI.FLOAT, Constantes.PROCESSO_MASTER, Constantes.REQUISITAR_MATRIZ_B);
            status = MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.CHAR, Constantes.PROCESSO_MASTER, Constantes.ENVIAR_MATRIZ_B);
            reader = new JsonReader(new StringReader(new String(buffer)));
            reader.setLenient(true);
            matrixB = gson.fromJson(reader,float[].class);

            MPI.COMM_WORLD.send(new float[1], 1, MPI.FLOAT, Constantes.PROCESSO_MASTER, Constantes.REQUISITAR_LINHA);

            int[] buffer2 = new int[2];

            status = MPI.COMM_WORLD.recv(buffer2, 2, MPI.INT, Constantes.PROCESSO_MASTER, Constantes.ENVIAR_LINHA);

            inicio = buffer2[0];
            fim = buffer2[1];

            //System.out.println(MPI.COMM_WORLD.getRank() + " " + inicio + " " + fim);

            resolver();

        } catch (MPIException e) {
            e.printStackTrace();
        }
    }

    public void resolver(){
        while(true){
            try {
                //double[] buffer = new double[100000];
                //System.out.println(" Esperando matrix C, processo " + MPI.COMM_WORLD.getRank());
                MPI.COMM_WORLD.send(new float[1], 1, MPI.FLOAT, Constantes.PROCESSO_MASTER, Constantes.REQUISITAR_MATRIZ_C);
                Status status = MPI.COMM_WORLD.recv(matrixC, matrixC.length, MPI.DOUBLE, Constantes.PROCESSO_MASTER, Constantes.ENVIAR_MATRIZ_C);
                if(status.getCount(MPI.DOUBLE) == 1) break;
            } catch (MPIException e) {
                e.printStackTrace();
            }
            int c = 0;

                //TODO para para a próxima iteracao
                //System.out.println("--Requisitando uma linha " + c++);
            try {
                double[] temp = new double[fim - inicio + 1 + 2];
                temp[0] = inicio;
                temp[1] = fim;
                for(int i = inicio, k = 2; i <= fim; i++, k++) {

                    double a = 0;
                    for (int j = 0; j < matrixA.length; j++) {
                        a += i == j ? (matrixB[i]) / (double) matrixA[i][i] : (matrixA[i][j] / (-matrixA[i][i])) * matrixC[j];
                    }

                    temp[k] = a;
                }

                MPI.COMM_WORLD.send(temp, temp.length, MPI.DOUBLE, Constantes.PROCESSO_MASTER, Constantes.RECEBER_RESPOSTA);

            } catch (MPIException e) {
                e.printStackTrace();
            }
        }

    }
}
