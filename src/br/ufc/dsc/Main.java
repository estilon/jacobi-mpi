package br.ufc.dsc;

import mpi.MPI;
import mpi.MPIException;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static int n;
    public static int numberOfThreadsByProcessor;
    public static int verificationLine;
    public static int maxNumberOfInteractions;

    public static void main(String[] args) throws MPIException{
        MPI.Init(args);
        long time;
        int myrank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize() ;

        if(myrank == 0) {

            lerEntradas();

        }else{
            Solver solver = new Solver();
        }

        MPI.Finalize();


    }

    private static void lerEntradas() {
        BufferedReader br = null;
        long time;
        try {
            time = System.currentTimeMillis();

            br = new BufferedReader(new FileReader("/home/estilon/IdeaProjects/Jacobi-MPI/matriz4.dat"));

            String line = null;

            n = Integer.parseInt(br.readLine());

            numberOfThreadsByProcessor = Integer.parseInt(br.readLine());
            verificationLine = Integer.parseInt(br.readLine());
            maxNumberOfInteractions = Integer.parseInt(br.readLine());

            Scanner scanner = new Scanner(br);

            float[][] matrixA = new float[n][n];
            float[] matrixB = new float[n];

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrixA[i][j] = scanner.nextFloat();
                }
            }

            for (int i = 0; i < n; i++) {
                matrixB[i] = scanner.nextFloat();
            }

            scanner.close();

            Jacobi jacobi = new Jacobi(matrixA, matrixB);

            PrintWriter pw = new PrintWriter(new FileOutputStream(
                    new File("saida.dat"),
                    true));
            if(jacobi.verificarCongruencia()){
                double [] matrixC = jacobi.raiz();
                for(int i = 0; i < n; i++){

                    pw.print(matrixC[i] + " ");
                }

                pw.println("\nNumero de iteracoes " + jacobi.iteracoes());

                pw.println("Diferença " + jacobi.diferenca(verificationLine));

                time = System.currentTimeMillis() - time;
                pw.println("Tempo de execução " + time/(double)1000);

            }else {
                pw.println("A matriz não converge");
            }

            pw.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
