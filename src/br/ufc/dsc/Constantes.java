package br.ufc.dsc;

/**
 * Created by estilon on 04/07/16.
 */
public class Constantes {

    public static final int ENVIAR_MATRIZ_A = 1 ;
    public static final int REQUISITAR_MATRIZ_A = 0;
    public static final int PROCESSO_MASTER = 0;
    public static final int ENVIAR_LINHA = 2;
    public static final int ENVIAR_MATRIZ_B = 3;
    public static final int REQUISITAR_MATRIZ_B = 4;
    public static final int REQUISITAR_LINHA = 5;
    public static final int RECEBER_RESPOSTA = 6;
    public static final int REQUISITAR_MATRIZ_C = 7;
    public static final int ENVIAR_MATRIZ_C = 8;
    public static final float FINALIZAR = -2;
}
